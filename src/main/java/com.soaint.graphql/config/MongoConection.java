package com.soaint.graphql.config;

import com.mongodb.ConnectionString;
import com.mongodb.async.client.*;
import org.bson.Document;


/**
 * Created by criptotec on 9/26/16.
 */
public class MongoConection {
    private ConnectionString            mongoConection;
    private MongoClient                 mongoClient;
    private MongoDatabase               database;

    public MongoConection() {
        mongoConection      = new ConnectionString("mongodb://127.0.0.1:27017");
        mongoClient         = MongoClients.create(mongoConection);
        database            = mongoClient.getDatabase("cursoMongo");
    }

    public MongoDatabase getDatabase() {
        return database;
    }

    public MongoCollection<Document> getCollection(String collection) {
        return database.getCollection(collection);
    }

    public void closeConection() {
        mongoClient.close();
    }
}
