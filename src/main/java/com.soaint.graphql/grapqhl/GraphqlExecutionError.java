package com.soaint.graphql.grapqhl;

import graphql.GraphQLError;

import java.util.List;

/**
 * Created by criptotec on 15/06/17.
 */
public class GraphqlExecutionError extends Throwable {
    private final List<GraphQLError> graphQLErrors;

    public GraphqlExecutionError(List<GraphQLError> errors) {
        graphQLErrors = errors;
    }

    public List<GraphQLError> getGraphQLErrors() {
        return graphQLErrors;
    }
}
