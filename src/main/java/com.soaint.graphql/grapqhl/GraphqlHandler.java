package com.soaint.graphql.grapqhl;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import com.jsoniter.output.JsonStream;
import com.mongodb.async.client.MongoDatabase;
import com.soaint.graphql.Services.ResolveUserServices;
import com.soaint.graphql.config.MongoConection;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.*;
import graphql.schema.idl.*;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;
import static java.lang.String.join;
//////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Created by criptotec on 05/06/17.
 */
public class GraphqlHandler {
    private final MongoConection mongo;
    private final MongoDatabase database;
    private final Vertx vertx;

    private String subject;

    public final GraphQL graphQL;

    Cache<String, String> cache;
    //////////////////////////////////////////////////////////////////////////////////////////////

    public GraphqlHandler(Vertx vertx) {
        this.vertx      = vertx;
        mongo           = new MongoConection();
        database        = mongo.getDatabase();

        cache = Caffeine.newBuilder()
                .maximumSize(10_000)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build();


        SchemaParser schemaParser = new SchemaParser();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQL graphQL_t;

        try {
            TypeDefinitionRegistry typeRegistry;
            typeRegistry = schemaParser.parse(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResource("graphql-schemas/crud_schema.graphqls").openStream()));
            RuntimeWiring wiring = buildRuntimeWiring();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeRegistry, wiring);
            setSubject(subject);

            graphQL_t = GraphQL.newGraphQL(graphQLSchema).build();
        } catch (IOException e) {
            e.printStackTrace();
            graphQL_t = null;
        }

        graphQL = graphQL_t;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    public void handler(RoutingContext routingContext) {
        final Map<String, Object> variables = new HashMap<>();
        String jsonQuery = "";
        final String queryParam = routingContext.request().getParam("query");
        final String variableParam = routingContext.request().getParam("variables");

        if(routingContext.request().method().name().equals("POST") && routingContext.getBodyAsString() != null) {
            final Any deserialize = JsonIterator.deserialize(routingContext.getBodyAsString());
            jsonQuery = deserialize.get("query").toString();
            if(deserialize.get("variables").size()>0)
            variables.putAll(deserialize.get("variables").asMap()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString())));
        } else {
            if(queryParam != null & variableParam != null) {
                jsonQuery = queryParam;
                variables.putAll(JsonIterator.deserialize(variableParam).asMap()
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString())));
            }
        }

        final String key = jsonQuery.replaceAll("\\s+", "");
        final String query = jsonQuery;

        String response = routingContext.request().method().equals(HttpMethod.GET) ? cache.getIfPresent(key) : null ;
        if (response == null) {
            routingContext.vertx().executeBlocking(future -> {
                Map<String, Object> result = new HashMap<>();
                String str;
                try {
                    ExecutionResult execution = graphQL.execute(query, routingContext, variables);
                    if (execution.getErrors().size() > 0) {
                        throw new GraphqlExecutionError(execution.getErrors());
                    }

                    str = JsonStream.serialize(execution.getData());
                    cache.put(key, str);
                } catch (Exception e) {
                    result.put("error",     true);
                    result.put("data",      null);
                    result.put("message",   e.getLocalizedMessage());

                    str = JsonStream.serialize(result);
                } catch (GraphqlExecutionError e) {
                    result.put("error",     true);
                    result.put("data",      null);
                    List<String> messages = new ArrayList<>();
                    e.getGraphQLErrors().forEach(error -> messages.add(error.getMessage()));
                    result.put("message", messages);

                    str = JsonStream.serialize(result);
                }
                future.complete(str);
                }, res -> {
                    // TODO: Manejar la devolución de errores
                    if (res.failed()) {
                    } else {
                        routingContext.request().response()
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .putHeader("Access-Control-Allow-Origin", "*")
                                .putHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                                .putHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
                                .end(res.result().toString());
                    }
                }
            );
        } else {
            routingContext.request().response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .putHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                    .putHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
                    .end(response);
        }

    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("QueryType", typeWiring -> typeWiring
                        .dataFetcher("getUsersData", ResolveUserServices.getUsersData(database))
                        .dataFetcher("getUserData", ResolveUserServices.getUserData(database))
                )
                .type("MutationType", typeWiring -> typeWiring
                        .dataFetcher("insertUser", ResolveUserServices.insert(database))
                        .dataFetcher("deleteUser", ResolveUserServices.deleteUser(database))
                        .dataFetcher("updateUserData", ResolveUserServices.updateUserData(database))
                )
                .type(
                        newTypeWiring("KeyValue")
                                .typeResolver(ResolveUserServices.getKeyValueResolver())
                                .build()
                )
                .build();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}