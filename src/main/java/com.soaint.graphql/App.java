package com.soaint.graphql;
import io.vertx.core.Vertx;

/**
 * Created by criptotec on 9/22/16.
 */

public class App  {

    public static void main(String[] args) {

        /*Instances verticles*/
        VerticleRouter          verticleRouter              = new VerticleRouter();


        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(verticleRouter);
        System.out.println("All services started...");

    }

}

