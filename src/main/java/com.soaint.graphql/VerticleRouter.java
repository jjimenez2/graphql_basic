package com.soaint.graphql;

import com.mongodb.async.client.MongoDatabase;
import com.soaint.graphql.config.MongoConection;
import com.soaint.graphql.grapqhl.GraphqlHandler;
import io.vertx.core.*;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;

import java.io.InputStream;

/**
 * Created by criptotec on 9/22/16.
 */
public class VerticleRouter extends AbstractVerticle {
    EventBus eb;
    HttpServer server;
    BodyHandler bodyHandler;
    Router router;
    GraphqlHandler graphqlHandler;

    /*Conexion a base de datos*/
    private final MongoDatabase database;
    private final MongoConection mongo;

    public VerticleRouter() {
        mongo                           = new MongoConection();
        database                        = mongo.getDatabase();
    }


    @Override
    public void start(Future<Void> startFuture) throws Exception {
        eb = vertx.eventBus();
        server = vertx.createHttpServer();
        bodyHandler = BodyHandler.create();
        router = Router.router(vertx);
        graphqlHandler = new GraphqlHandler(vertx);

        router.route().handler(bodyHandler);
        bodyHandler.setUploadsDirectory(System.getenv("UPLOAD_DIRECTORY"));

        Route healthcheck = router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            StringBuilder stringResp = new StringBuilder();

            try {
                response.setStatusCode(200);
                stringResp.append("<html>");
                InputStream is = this.getClass().getResourceAsStream("/version");
                stringResp.append("Service version:");
                stringResp.append(convertStreamToString(is));
                is.close();
                stringResp.append("</html>");
                response.end(stringResp.toString());
            } catch (Exception e) {
                response.setStatusCode(500);
                stringResp.append("</br>ERROR:" + e.getMessage());
                response.end(stringResp.toString());
            }

        });


        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
//        router.route().handler(UserSessionHandler.create(authProvider));
//        router.route("/api/graphql").handler(JWTAuthHandler.create(authProvider));
        router.route("/api/graphql").blockingHandler(graphqlHandler::handler, false);
        router.route("/api/graphql").handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("X-PINGARUNER")
                .allowedHeader("Content-Type"));


        router.route().handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("X-PINGARUNER")
                .allowedHeader("cache-control")
                .allowedHeader("x-requested-with")
                .allowedHeader("Content-Type"));

        router.get("/access-control-with-get").handler(ctx -> {

            ctx.response().setChunked(true);

            MultiMap headers = ctx.request().headers();
            for (String key : headers.names()) {
                ctx.response().write(key);
                ctx.response().write(headers.get(key));
                ctx.response().write("\n");
            }

            ctx.response().end();
        });

        router.post("/access-control-with-post-preflight").handler(ctx -> {
            ctx.response().setChunked(true);

            MultiMap headers = ctx.request().headers();
            for (String key : headers.names()) {
                ctx.response().write(key);
                ctx.response().write(headers.get(key));
                ctx.response().write("\n");
            }

            ctx.response().end();
        });

        server.requestHandler(router::accept).listen(8081);
        startFuture.complete();
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


    public void stop() {
    }

}
