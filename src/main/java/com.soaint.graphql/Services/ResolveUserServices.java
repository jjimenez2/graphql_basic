package com.soaint.graphql.Services;

import com.jsoniter.JsonIterator;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.sun.org.apache.xpath.internal.operations.Bool;
import graphql.TypeResolutionEnvironment;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLObjectType;
import graphql.schema.TypeResolver;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ResolveUserServices {

    public static TypeResolver getKeyValueResolver() {
        return new TypeResolver() {
            @Override
            public GraphQLObjectType getType(TypeResolutionEnvironment env) {
                return null;
            }
        };
    }

    public static DataFetcher getUsersData(MongoDatabase database) {
        return env -> {

            System.out.println("Estoy en el metodo get users data");

            CompletableFuture<List<Document>>   future      = new CompletableFuture<>(); // <-- create an empty, uncompleted Future
            MongoCollection<Document>           collection  = database.getCollection("users");

            collection.find().into(new ArrayList<Document>(),
                    (SingleResultCallback<List<Document>>) (result, t) -> {
                        if(result.size()>0){
                            future.complete(result);
                        }
                        else
                            future.complete(null);
                    });
            try {
                return future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        };
    }

    public static DataFetcher insert(MongoDatabase mongo) {
        return env -> {
            System.out.println("Estoy en el metodo insert");
            try {
                Map input               = JsonIterator.deserialize(env.getArgument("input").toString(), Map.class);
                Document newRegister    = new Document();
                Document response       = new Document();
                String   errorMessage   = "";
                response.append("accepted", false);


                if(input.get("firstname")==null)
                    errorMessage += "First Name can't be null,";

                if(input.get("lastname")==null)
                    errorMessage += "Last Name can't be null,";

                if(input.get("cedula")==null)
                    errorMessage += "Cedula can't be null,";

                if(input.get("phone")==null)
                    errorMessage += "Phone can't be null,";

                if(input.get("location")==null)
                    errorMessage += "Location can't be null,";

                if(input.get("password")==null)
                    errorMessage += "Password can't be null,";

                if(input.get("birthdate")==null)
                    errorMessage += "Birthdate can't be null,";

                if(input.get("country")==null)
                    errorMessage += "Country can't be null,";

                if(!errorMessage.equals("")){
                    response.append("error", errorMessage);
                    return  response;
                }

                newRegister.append("firstname"  , (String) input.get("firstname"));
                newRegister.append("lastname"   , (String) input.get("lastname"));
                newRegister.append("cedula"     , (String) input.get("cedula"));
                newRegister.append("phone"      , (String) input.get("phone"));
                newRegister.append("location"   , (String) input.get("location"));
                newRegister.append("password"   , (String) input.get("password"));
                newRegister.append("birthdate"  , (String) input.get("birthdate"));
                newRegister.append("country"    , (String) input.get("country"));
                newRegister.append("id"         , new ObjectId().toString());

                CompletableFuture<Boolean> future = new CompletableFuture<>(); // <-- create an empty, uncompleted Future

                MongoCollection<Document> collection = mongo.getCollection("users");

                collection.insertOne(newRegister, (result, t) -> {
                        if(t==null){
                            future.complete(true);
                        }
                        else{
                            future.complete(false);
                        }
                });

                response.append("accepted", future.get());
                return response;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            } catch (ExecutionException e) {
                e.printStackTrace();
                return false;
            }
        };
    }


    public static DataFetcher getUserData(MongoDatabase database){
        return env -> {
            System.out.println("Estoy en el metodo get User Data");


            Map input = JsonIterator.deserialize(env.getArgument("input").toString(), Map.class);

            if(input.get("cedula")==null)
                return null;

            Document filter = new Document().append("cedula", (String) input.get("cedula"));

            CompletableFuture<Document> future      = new CompletableFuture<>();
            MongoCollection<Document> collection    = database.getCollection("users");

            collection.find(filter).into(new ArrayList<Document>(),
                    (SingleResultCallback<List<Document>>) (result, t) -> {
                        if (result.size() > 0) {
                            future.complete(result.get(0));
                        } else
                            future.complete(null);
                    });
            try {
                return future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        };
    }

    public static DataFetcher updateUserData(MongoDatabase database){
        return env -> {
            System.out.println("Estoy en el metodo update User Data");

            Map             input           = JsonIterator.deserialize(env.getArgument("input").toString(), Map.class);
            Document        response        = new Document();
            Document        updatedField    = new Document();

            response.append("accepted", false);

            if(input.get("cedula")==null){
                response.append("error", "Cedula can't be null");
                return response;
            }

            Document filter = new Document().append("cedula", (String) input.get("cedula"));


            if(input.get("firstname")!=null)
                updatedField.append("firstname", input.get("firstname"));

            if(input.get("lastname")!=null)
                updatedField.append("lastname", input.get("lastname"));

            if(input.get("phone")!=null)
                updatedField.append("phone", input.get("phone"));

            if(input.get("location")!=null)
                updatedField.append("location", input.get("location"));

            if(input.get("password")!=null)
                updatedField.append("password", input.get("password"));

            if(input.get("birthdate")!=null)
                updatedField.append("birthdate", input.get("birthdate"));

            if(input.get("country")!=null)
                updatedField.append("country",  input.get("country"));


            if(updatedField.size()==0){
                response.append("error", "Debe ingresar al menos un campo para actualizar");
                return  response;
            }

            Document response1    = new Document();

            CompletableFuture<Boolean> future      = new CompletableFuture<>();
            MongoCollection<Document> collection    = database.getCollection("users");
            collection.updateOne(filter, new Document().append("$set",updatedField), (result, t) -> {
                 if(t==null){
                        future.complete(true);
                 }
                 else{
                     response1.append("error", t);
                     future.complete(false);
                 }
            });
            try {
                response1.append("accepted", future.get());
                return response1;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        };
    }


    public static DataFetcher deleteUser(MongoDatabase database) {
        return env -> {
            System.out.println("Estoy en el metodo delete user");

            CompletableFuture<Boolean> future    = new CompletableFuture<>(); // <-- create an empty, uncompleted Future
            MongoCollection<Document> collection = database.getCollection("users");

            Map input = JsonIterator.deserialize(env.getArgument("input").toString(), Map.class);

            if(input.get("cedula")==null) {
                Document response = new Document();
                response.append("accepted", false);
                response.append("error","Cedula can't be null");
                return response;
            }

            Document filter     = new Document().append("cedula", (String) input.get("cedula"));
            Document response   = new Document();

            collection.deleteOne(filter, (result, t) -> {
                if(t==null){
                    future.complete(true);
                }
                else{
                    future.complete(false);
                    response.append("error", t.toString());
                }
            });
            try {
                response.append("accepted", future.get());
                return response;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        };
    }
}
